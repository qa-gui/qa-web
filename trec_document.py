#!/usr/bin/env python

from document import Document


class TrecDocument(Document):
    """Document for the TREC BioQA task"""
    def __init__(self, uid=None, uri=None, rank=None, contents=None):
        super(TrecDocument, self).__init__(uid=uid, uri=uri, rank=rank,
                                           contents=contents)

    def highlight(self, begin, end):
        self.contents = ''.join([
            self.contents[:begin],
            '<span id="highlight" class="highlight">',
            self.contents[begin:end],
            '</span>',
            self.contents[end:],
        ])
