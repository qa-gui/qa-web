#!/usr/bin/env python


from flask import (Flask, render_template, g, redirect, url_for, Response,
                   flash, request)
from flaskext.mysql import MySQL
from flask.ext.login import (LoginManager, login_required, login_user,
                             logout_user)

import sys
import json
import zlib
import yaml
import _mysql_exceptions

import graph_helper
from user import User, Anonymous

try:
    CONFIG = yaml.load(open('config.yaml'))
except:
    sys.stderr.write('You need a config.yaml file\n')
    sys.exit(1)

# Use some metaprogramming magic to import factories for CAS and Document
CasFactory = getattr(__import__(CONFIG['model']['cas']['file']),
                     CONFIG['model']['cas']['class'])
DocumentFactory = getattr(__import__(CONFIG['model']['document']['file']),
                          CONFIG['model']['document']['class'])

app = Flask(__name__)
app.config['MYSQL_DATABASE_HOST'] = CONFIG['database']['host']
app.config['MYSQL_DATABASE_PORT'] = CONFIG['database']['port']
app.config['MYSQL_DATABASE_USER'] = CONFIG['database']['user']
app.config['MYSQL_DATABASE_PASSWORD'] = CONFIG['database']['password']
app.config['MYSQL_DATABASE_DB'] = CONFIG['database']['db']
app.config['SECRET_KEY'] = CONFIG['secret_key']

mysql = MySQL()
mysql.init_app(app)

login_manager = LoginManager()
login_manager.anonymous_user = Anonymous
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"


@login_manager.user_loader
def load_user(id):
    try:
        cur = connect_db()
        cur.execute("SELECT name from user where id = %s", id)
        row = cur.fetchone()
        username = row[0]
        cur.close()
        user = User(username, id)
        return user
    except:
        return None

login_manager.setup_app(app)


def connect_db():
    return mysql.get_db().cursor()


def commit():
    mysql.get_db().commit()


@app.before_request
def before_request():
    g.cursor = connect_db()


@app.after_request
def after_request(response):
    g.cursor.close()
    return response


@app.route("/")
def index():
    return redirect(url_for('experiments'))


@app.route("/experiments")
def experiments():
    cur = g.cursor
    cur.execute('SELECT id, name, author, date, size '
                'FROM experiment ORDER BY date DESC')
    experiments = [dict(id=row[0], name=row[1], author=row[2],
                        date=row[3], size=row[4])
                   for row in cur.fetchall()]
    return render_template('experiments.html', experiments=experiments)


@app.route('/experiment/<experiment_id>')
def experiment(experiment_id):
    return render_template('experiment.html', experiment_id=experiment_id)


@app.route('/graph/<experiment_id>')
def graph(experiment_id):
    cur = g.cursor
    cur.execute("SELECT DISTINCT trace FROM cas_str WHERE experiment = %s",
                experiment_id)
    rows = cur.fetchall()
    trace_strs = map(lambda row: row[0], rows)
    js = graph_helper.construct_graph(trace_strs)
    return Response(json.dumps(js),  mimetype='application/json')


@app.route('/cases/<experiment_id>/<path:trace>')
def cases(experiment_id, trace):
    cur = g.cursor
    cur.execute("SELECT id FROM cas_str WHERE experiment = %s and trace = %s",
                (experiment_id, trace))
    cases = [dict(id=row[0]) for row in cur.fetchall()]
    return render_template('cases.html', cases=cases,
                           experiment_id=experiment_id, trace=trace)


@app.route('/cas/<cas_id>')
@login_required
def cas(cas_id):
    cur = g.cursor
    cur.execute("SELECT xcas from cas_str where id = %s", cas_id)
    try:
        row = cur.fetchone()
        cas_gzipped = row[0]
    except:
        error_msg = 'No corresponding CAS found'
        return render_template('error.html', error_msg=error_msg)

    try:
        xmi = zlib.decompress(cas_gzipped, 16 + zlib.MAX_WBITS)
    except:
        error_msg = 'There was an error in decompressing the CAS'
        return render_template('error.html', error_msg=error_msg)

    cas = CasFactory().create_cas(uid=cas_id, xmi=xmi)
    return render_template(CONFIG['view']['cas']['file'], cas=cas)


@app.route('/passage/<uri>/<begin>/<end>')
def passage(uri, begin, end):
    try:
        doc = DocumentFactory().create_document(uri=uri)
        b = int(begin)
        e = int(end)
        doc.highlight(b, e)
        return Response(json.dumps(doc.as_js()),  mimetype='application/json')
    except:
        return Response(json.dumps({
            'contents': 'There was an error in retrieving the passage'
        }),  mimetype='application/json')


@app.route('/document/<uri>')
def document(uri):
    try:
        doc = DocumentFactory().create_document(uri=uri)
        return Response(json.dumps(doc.as_js()),  mimetype='application/json')
    except:
        return Response(json.dumps({
            'contents': 'There was an error in retrieving the document'
        }),  mimetype='application/json')


@app.route('/decorate_binary', methods=["POST"])
def decorate_binary():
    try:
        cas_id = request.form["cas_id"]
        xmi_id = request.form["xmi_id"]
        user_id = request.form["user_id"]
        value = request.form["value"]

        cur = g.cursor
        cur.execute("INSERT INTO decoration_binary "
                    "(casId, xmiId, userId, value) VALUES (%s, %s, %s, %s)",
                    (cas_id, xmi_id, user_id, value))
        commit()
        return Response(json.dumps(True),  mimetype='application/json')
    except:
        return Response(json.dumps(False),  mimetype='application/json')


@app.route('/decorate_real', methods=["POST"])
def decorate_real():
    try:
        cas_id = request.form["cas_id"]
        xmi_id = request.form["xmi_id"]
        user_id = request.form["user_id"]
        value = request.form["value"]

        cur = g.cursor
        cur.execute("INSERT INTO decoration_real"
                    "(casId, xmiId, userId, value) "
                    "VALUES (%s, %s, %s, %s)",
                    (cas_id, xmi_id, user_id, value))
        commit()
        return Response(json.dumps(True),  mimetype='application/json')
    except:
        return Response(json.dumps(False),  mimetype='application/json')


@app.route('/decorate_span', methods=["POST"])
def decorate_span():
    commit()
    try:
        cas_id = request.form["cas_id"]
        xmi_id = request.form["xmi_id"]
        user_id = request.form["user_id"]
        span = request.form["span"]
        cur = g.cursor
        cur.execute("INSERT INTO decoration_span (casId, xmiId, userId, span) "
                    "VALUES (%s, %s, %s, %s)",
                    (cas_id, xmi_id, user_id, span))
        commit()
        return Response(json.dumps(True),  mimetype='application/json')
    except:
        return Response(json.dumps(False),  mimetype='application/json')


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        try:
            cur = g.cursor
            cur.execute("SELECT id from user where name = %s", username)
            row = cur.fetchone()
            user_id = int(row[0])
        except:
            flash(u"Invalid username.")
            return render_template("login.html")

        user = User(username, user_id)
        remember = request.form.get("remember", "no") == "yes"
        if login_user(user, remember=remember):
            flash("Successfully logged in!")
            return redirect(request.args.get("next") or url_for("index"))
        else:
            flash("Sorry, but you could not log in.")
    return render_template("login.html")


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        try:
            cur = g.cursor
            cur.execute("INSERT INTO user (name) values (%s)", username)
            commit()
            flash("Successfully signed up!")
            return redirect(request.args.get("next") or url_for("index"))
        except _mysql_exceptions.IntegrityError:
            error_msg = "The username '%s' already exists" % username
            return render_template('error.html', error_msg=error_msg)
        except:
            error_msg = "There was an error signing you up!"
            return render_template('error.html', error_msg=error_msg)
    return render_template("signup.html")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Successfully logged out.")
    return redirect(url_for("index"))


if __name__ == "__main__":
    app.debug = True
    app.run()
