# qa-web

## Usage

1. Write a `config.yaml` (explained later)
2. run `python app.py`
3. Point your browser to http://localhost:5000.

By default, the app will be only visible to your own computer,
and not from any other machine. You can make the server publicly
available simply by changing `app.run()` to: `app.run(host='0.0.0.0')`
in `app.py`.

## Dependencies

* [Flask](http://flask.pocoo.org/)
* [Flask-MySQL](https://flask-mysql.readthedocs.org/en/latest/)
* [Flask-Login](https://pypi.python.org/pypi/Flask-Login)
* [PyYAML](http://pyyaml.org/wiki/PyYAMLDocumentation)

To install Flask, Flask-MySQL, and FLask-Login, run the following command:

    pip install Flask flask-mysql flask-login

## Configuration

Write a `config.yaml` file that looks like the following:

    view:
      cas:
        file: trec_cas.html
    model:
      cas:
        file: trec_cas_factory
        class: TrecCasFactory
      document:
        file: trec_document_factory
        class: TrecDocumentFactory
    database:
      host: localhost
      port: 3306
      user: foo
      password: bar
      db: baz
    secret_key: 1234abcd

The key `view` specifies the "View" in [MVC](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller).
The key `model` specifies the "Model" in MVC. The specification for the subkeys `cas` and `document` is explained in the following section.

The key `database` specifies the connection parameters for the SQL database used for both retrieving CASes and saving annotations.

The key `secret_key` is used for session management (logging in, logging out, and remembering user's sessions).



## Customization

By default, this web app is used to show and annotate CASes (with the XMI format) from the TREC BioQA task, 
but is also general enough to deal with CASes generated from other QA tasks.
The application uses the [factory method](http://en.wikipedia.org/wiki/Factory_method_pattern) pattern to handle this.

Refer to the following diagram:
![Factory method pattern](https://bitbucket.org/qa-gui/qa-web/raw/master/factory.png)
To view and annotate (serialized) CASes with a different XML MetaData Interchange format
(possibly from a different QA task), one only needs to subclass the `CasFactory` and
`Cas` classes, providing concrete implementations for the
`produceCas`, `extractPassages`, `extractKeywords`, and `extractDocuments`
methods.
The client calls the `create` method on a `CasFactory` instance, which in
turn calls the factory method `produceCas`.
`extractPassages`, `extractKeywords`, and `extractDocuments`
are template methods called within `create`,
which extracts `Passage`s, `Keyword`s, and `Document`s, respectively, from the XMI.

Refer to `trec_cas_factory.py` for an example factory implementation.

The keys subkeys `cas` and `document` (within the `model` namespace) in `config.yaml` specify the python file and class used to load CASes and documents, respectively.
For example, 

    model:
      cas:
        file: trec_cas_factory
        class: TrecCasFactory
      
would look for the class `TrecCasFactory` in `trec_cas_factory.py`.

You can also customize the view for a CAS. Simply subclass the base view, `templates/cas.html` and modify the value for the key `view` in the yaml file accordingly.
For more information, refer to [jinja2's template inheritance](http://jinja.pocoo.org/docs/templates/#template-inheritance) mechanism.