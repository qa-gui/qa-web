#!/usr/bin/env python


class Cas(object):
    """Base class that all Cases should derive from"""
    def __init__(self, uid=None, question=None, documents=[],
                 passages=[], keywords=[]):
        self.uid = uid
        self.question = question
        self.documents = documents
        self.passages = passages
        self.keywords = keywords
