#!/usr/bin/env python

"""
Data structures that represent artifacts.
(These are currently not written as full-blown classes
because they are simple value classes)
"""

from collections import namedtuple

"""
Document
--------
id:    xmi:id="23797"
uri:   uri="10901330"
rank:  rank="531"
"""
Document = namedtuple('Document', 'id uri rank')

"""
Passage
--------
id:    xmi:id="49081"
uri:   uri="15632190"
begin: begin="9885"
end:   end="10576"
rank:  rank="1"
"""
Passage = namedtuple('Passage', 'id uri begin end rank')

"""
Keyword
--------
id:    xmi:id="3082"
text:  text="amyloid"
"""
Keyword = namedtuple('Keyword', 'id text')

"""
Question
--------
text:   sofaString="What is the role of Transforming growth
                    factor-beta1 (TGF-beta1) in cerebral
                    amyloid angiopathy (CAA)?"
"""
Question = namedtuple('Question', 'id text')
