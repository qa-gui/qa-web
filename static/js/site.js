$(function() {
  Messenger.options = {
    extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
    theme: 'future'
  }

  var selectedRow = false;
  var submittedRows = {};

  $('.scrollable li [href^=#]').click(function (e) {
    $('.buttons').show();
    // var a = $(this);
    // var selectedRow = a.parent();
    // console.log(selectedRow);
    // if (!(selectedRow in submittedRows)) {
    //   resetButtons();
    // }
    e.preventDefault();
  });

  var resetButtons = function() {
    $(".btn-danger").removeClass("disabled");
    $(".btn-success").removeClass("disabled");
  };

  /* Keyword */
  $('.keyword-item[href^=#]').click(function (e) {
    $(selectedRow).removeClass('selected');
    selectedRow = $(this).parent();
    $(selectedRow).addClass('selected');

    showKeyword(this);
  });

  var showKeyword = function(a) {
    var keyword = $(a).data('keyword');
    $('#pane').html('<h2>' + keyword + '</h2>');
  }

  /* Document */
  $('.document-item[href^=#]').click(function (e) {
    $(selectedRow).removeClass('selected');
    selectedRow = $(this).parent();
    $(selectedRow).addClass('selected');

    showDocument(this);
  });

  var showDocument = function(a) {
    var uri = $(a).data('uri');
    var url = "/document/" + uri;
    $.ajax(url).done(function(json) {
      var pane = $('#pane');
      pane.html(json['contents']);
      var scrollTarget = pane.find('h2');
      pane.stop().scrollTo(scrollTarget, 200);
    });
  };

  /* Passage */
  $('.passage-item[href^=#]').click(function (e) {
    $(selectedRow).removeClass('selected');
    selectedRow = $(this).parent();
    $(selectedRow).addClass('selected');
    showPassage(this);
  });

  var showPassage = function(a) {
    var uri = $(a).data('uri'),
        begin = $(a).data('begin'),
        end = $(a).data('end');

    var url = "/passage/" + uri + "/" + begin + "/" + end;
    $.ajax(url).done(function(json) {
      var pane = $('#pane');
      pane.html(json['contents']);
      var scrollTarget = pane.find('#highlight');
      pane.stop().scrollTo(scrollTarget, 800);
    });
  };

  var nextEntry = function() {
    if (! selectedRow) {
      selectedRow = $('div.scrollable section ul li:first');
      $(selectedRow).addClass('selected');
      var a = $(selectedRow).children("a");
      a.click();
    }
    else {
      $(selectedRow).removeClass('selected');
      selectedRow = $(selectedRow).next('li');
      $(selectedRow).addClass('selected');
      var a = $(selectedRow).children("a");
      a.click();
    }
  };

  var prevEntry = function() {
    if (! selectedRow) {
      selectedRow = $('div.scrollable section ul li:first');
      $(selectedRow).addClass('selected');
      var a = $(selectedRow).children("a");
      a.click();
    }
    else {
      $(selectedRow).removeClass('selected');
      selectedRow = $(selectedRow).prev('li');
      $(selectedRow).addClass('selected');
      var a = $(selectedRow).children("a");
      a.click();
    }
  };


  /*
   * Buttons
   */

  /* Binary */
  $('#btn-relevant').click(function() {
    // if (selectedRow in submittedRows) {
    //   Messenger().post({
    //     message: "You've already submitted for this entry!",
    //     type: 'error',
    //     showCloseButton: true
    //   });
    //   return;
    // }
    // submittedRows[selectedRow] = true;

    var btn = $(this);
    // btn.button('loading');
    var a = $(selectedRow).children("a");

    var cas_id = $('#cas').data('casid');
    var xmi_id = $(a).data('xmiid');
    var user_id = $('#user').data('userid');
    var value = 1;

    decorateBinary(cas_id, xmi_id, user_id, value);

    // animation
    // btn.button('reset');
    // $(".btn-danger").addClass("disabled");
    // $(".btn-success").addClass("disabled");
  });

  $('#btn-notrelevant').click(function() {
    var btn = $(this);
    // btn.button('loading');
    var a = $(selectedRow).children("a");

    var cas_id = $('#cas').data('casid');
    var xmi_id = $(a).data('xmiid');
    var user_id = $('#user').data('userid');
    var value = 0;
    decorateBinary(cas_id, xmi_id, user_id, value);
  });

  /* Real */
  $('button.btn-real').click(function() {
    // $(this).button('loading');
    var a = $(selectedRow).children("a");
    var cas_id = $('#cas').data('casid');
    var xmi_id = $(a).data('xmiid');
    var user_id = $('#user').data('userid');
    var value = $('#real-value').val();
    decorateReal(cas_id, xmi_id, user_id, value);

    // Temporarily remove parsley.js constraint
    $('#real-value').parsley('removeConstraint', 'required');
    $('#real-value').val('');
    // Add back constraint
    $('#real-value').parsley('addConstraint', 'required');
    $('#cas-real-button').attr('disabled', 'disabled');
  });

  /* Span */
  $('button.btn-interval').click(function() {
    // $(this).button('loading');
    var a = $(selectedRow).children("a");
    var cas_id = $('#cas').data('casid');
    var xmi_id = $(a).data('xmiid');
    var user_id = $('#user').data('userid');
    var span_text = $('#span-text').val();
    decorateSpan(cas_id, xmi_id, user_id, span_text);

    // Temporarily remove parsley.js constraint
    $('#span-text').parsley('removeConstraint', 'required');
    $('#span-text').val('');
    // Add back constraint
    $('#span-text').parsley('addConstraint', 'required');
    $('#cas-span-button').attr('disabled', 'disabled');
  });

  var decorateBinary = function(cas_id, xmi_id, user_id, value) {
    var url = "/decorate_binary";
    $.post(url, {
      cas_id: cas_id,
      xmi_id: xmi_id,
      user_id: user_id,
      value: value
    }).done(function(json) {
      if (json) {
        Messenger().post({
          message: "Your request has succeeded!",
          hideAfter: 4
        });
      } else {
        Messenger().post({
          message: "Your request has failed!",
          type: 'error',
          hideAfter: 4
        });
      }
    });
  }

  var decorateReal = function(cas_id, xmi_id, user_id, value) {
    var url = "/decorate_real";
    $.post(url, {
      cas_id: cas_id,
      xmi_id: xmi_id,
      user_id: user_id,
      value: value
    }).done(function(json) {
      if (json) {
        Messenger().post({
          message: "Your request has succeeded!",
          hideAfter: 4
        });
      } else {
        Messenger().post({
          message: "Your request has failed!",
          type: 'error',
          hideAfter: 4
        });
      }
    });
  }

  var decorateSpan = function(cas_id, xmi_id, user_id, span_text) {
    var url = "/decorate_span";
    $.post(url, {
      cas_id: cas_id,
      xmi_id: xmi_id,
      user_id: user_id,
      span: span_text
    }).done(function(json) {
      if (json) {
        Messenger().post({
          message: "Your request has succeeded!",
          hideAfter: 4
        });
      } else {
        Messenger().post({
          message: "Your request has failed!",
          type: 'error',
          hideAfter: 4
        });
      }
    });
  }

  /*
   * Parsley
   */
  $('#form-cas-real').parsley('addListener', {
    onFieldSuccess: function(elem) {
      $('#cas-real-button').removeAttr('disabled');
    },
    onFieldError: function(elem) {
      $('#cas-real-button').attr('disabled', 'disabled');
    }
  });

  $('#form-cas-span').parsley('addListener', {
    onFieldSuccess: function(elem) {
      $('#cas-span-button').removeAttr('disabled');
    },
    onFieldError: function(elem) {
      $('#cas-span-button').attr('disabled', 'disabled');
    }
  });

  var swapDecorationType = function() {
    var currentTab = $('ul#myTab li.active');
    var nextTab = currentTab.next().length > 0 ? currentTab.next() : $('ul#myTab li:first').first();
    var currentA = $(currentTab).children('a');
    var nextA = $(nextTab).children('a');
    nextA.tab('show');
  };

  var isRelevant = function() {
    if (! selectedRow) return;
    $('#btn-relevant').click();
  };

  var notRelevant = function() {
    if (! selectedRow) return;
    $('#btn-notrelevant').click();
  };

  var getSelectedElement = function() {
    var t = null;

    if (window.getSelection) {
      t = window.getSelection();
    }
    else if (document.getSelection) {
      t = document.getSelection();
    }
    else if (document.selection) {
      t = document.selection.createRange();
    }
    return t;
  }

  var fillSpanText = function() {
    var element = getSelectedElement();
    var text = element.toString();
    var nonWhiteSpaceText = text.replace(/\s/g, "");

    // Check if the selected element is inside the pane
    if (nonWhiteSpaceText.length > 0 &&
        ($(element.focusNode).parents().index($('#pane')) >= 0)) {
      // Temporarily remove parsley.js constraint
      $('#span-text').parsley('removeConstraint', 'required');
      $('#span-text').val(text);

      // Add back constraint
      $('#span-text').parsley('addConstraint', 'required');
      $('#cas-span-button').removeAttr('disabled');
    }
  }

  $(document).bind('keyup', 'j', nextEntry);
  $(document).bind('keyup', 'k', prevEntry);
  $(document).bind('keyup', 't', swapDecorationType);

  $(document).bind('keyup', 'y', isRelevant);
  $(document).bind('keyup', 'n', notRelevant);

  $(document).bind('mouseup', fillSpanText);
});
