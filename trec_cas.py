#!/usr/bin/env python

from cas import Cas


class TrecCas(Cas):
    """CAS for the TREC BioQA task"""
    def __init__(self, uid=None, question=None, documents=[],
                 passages=[], keywords=[]):
        super(TrecCas, self).__init__(uid=uid, question=question,
                                      documents=documents,
                                      passages=passages, keywords=keywords)
        self.documents = sorted(self.documents, key=lambda doc: doc.rank)
        self.passages = sorted(self.passages, key=lambda passage: passage.rank)
