-- schema.sql

DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id int NOT NULL auto_increment,
  name varchar(100) NOT NULL default '',
  registerDate timestamp NOT NULL default CURRENT_TIMESTAMP,
  groupId int NOT NULL default 0,
  PRIMARY KEY (id),
  UNIQUE KEY (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS decoration_binary;
CREATE TABLE decoration_binary (
  id int NOT NULL auto_increment,
  casId int NOT NULL,
  xmiId int NOT NULL,
  userId int NOT NULL,
  value tinyint(1) NOT NULL,
  date timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS decoration_real;
CREATE TABLE decoration_real (
  id int NOT NULL auto_increment,
  casId int NOT NULL,
  xmiId int NOT NULL,
  userId int NOT NULL,
  value float NOT NULL,
  date timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS decoration_span;
CREATE TABLE decoration_span (
  id int NOT NULL auto_increment,
  casId int NOT NULL,
  xmiId int NOT NULL,
  userId int NOT NULL,
  span text NOT NULL,
  date timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

