#!/usr/bin/env python

import xml.etree.ElementTree as et

from cas_factory import CasFactory
from trec_cas import TrecCas
from trec_document import TrecDocument
from structures import Passage, Keyword, Question


class TrecCasFactory(CasFactory):
    """CAS factory for the TREC BioQA task"""

    MODEL_NAMESPACE = 'http:///org/oaqa/model.ecore'
    OAQA_NAMESPACE = 'http:///edu/cmu/lti/oaqa.ecore'
    XMI_NAMESPACE = 'http://www.omg.org/XMI'
    CAS_NAMESPACE = 'http:///uima/cas.ecore'
    TYPES_NAMESPACE = 'http:///edu/cmu/lti/oaqa/framework/types.ecore'

    def _init_cas(self, uid=None, question=None, documents=[],
                  passages=[], keywords=[]):
        return TrecCas(uid=uid, question=question, documents=documents,
                       passages=passages, keywords=keywords)

    def _extract_documents(self, xmi):
        root = et.fromstring(xmi)

        document_ids = self._extract_corresponding_ids(root, artifact_type='DOCUMENT')
        if not document_ids:
            return []

        documents = []
        for node in root.findall('./{%s}Passage' % self.MODEL_NAMESPACE):
            xmi_id = node.attrib['{%s}id' % self.XMI_NAMESPACE]
            if not xmi_id in document_ids:
                continue

            document = TrecDocument(uid=node.attrib['{%s}id' %
                                    self.XMI_NAMESPACE],
                                    uri=node.attrib['uri'],
                                    rank=int(node.attrib['rank']))
            documents.append(document)
        return documents

    def _extract_passages(self, xmi):
        root = et.fromstring(xmi)

        passage_ids = self._extract_corresponding_ids(root, artifact_type='CANDIDATE')
        if not passage_ids:
            return []

        passages = []
        for node in root.findall('./{%s}Passage' % self.MODEL_NAMESPACE):
            xmi_id = node.attrib['{%s}id' % self.XMI_NAMESPACE]
            if not xmi_id in passage_ids:
                continue

            passage = Passage(
                id=node.attrib['{%s}id' % self.XMI_NAMESPACE],
                uri=node.attrib['uri'],
                begin=node.attrib['begin'],
                end=node.attrib['end'],
                rank=int(node.attrib['rank']),
            )
            passages.append(passage)
        return passages

    def _extract_keywords(self, xmi):
        root = et.fromstring(xmi)
        nodes = root.findall('./{%s}Token' % self.OAQA_NAMESPACE)
        return [Keyword(id=node.attrib['{%s}id' % self.XMI_NAMESPACE],
                        text=node.attrib['text']) for node in nodes]

    def _extract_question(self, xmi):
        root = et.fromstring(xmi)
        node = root.find("./{%s}InputElement" % self.TYPES_NAMESPACE)
        id = node.attrib['sequenceId']
        text = node.attrib['question']
        return Question(id=id, text=text)

    def _extract_corresponding_ids(self, root, artifact_type):
        assert artifact_type in ['CANDIDATE', 'DOCUMENT']

        try:
            node = root.find("./{%s}Sofa[@sofaID='%s']" %
                             (self.CAS_NAMESPACE, artifact_type))
            view_xmi_id = node.attrib['{%s}id' % self.XMI_NAMESPACE]
            view_node = root.find("./{%s}View[@sofa='%s']" %
                                  (self.CAS_NAMESPACE, view_xmi_id))
            members_xmi_id = view_node.attrib['members']

            node = root.find("./{%s}Search[@{%s}id='%s']" %
                             (self.MODEL_NAMESPACE, self.XMI_NAMESPACE,
                              members_xmi_id))
            ids = set(node.attrib['hitList'].split())
            return ids
        except:
            return set()
