#!/usr/bin/env python


class CasFactory(object):
    """Base class that all CAS factories should derive from.

    Subclasses must implement `_init_cas`, `_extract_documents`,
    `_extract_keywords`, `_extract_passages`, `_extract_keyword`,
    and `_extract_question`.
    """
    def __init__(self):
        pass

    def create_cas(self, uid=None, xmi=None):
        """Creates a new CAS, calling various template methods"""
        question = self._extract_question(xmi)
        documents = self._extract_documents(xmi)
        passages = self._extract_passages(xmi)
        keywords = self._extract_keywords(xmi)
        cas = self._init_cas(uid=uid, question=question, documents=documents,
                             passages=passages, keywords=keywords)
        return cas

    def _init_cas(self, uid=None, question=None, documents=[], passages=[],
                  keywords=[]):
        """Factory method for creating a CAS"""
        raise NotImplementedError()

    def _extract_question(self, xmi):
        """A template method that returns a `Question`,
        given a string-like representation of the xmi"""
        raise NotImplementedError()

    def _extract_documents(self, xmi):
        """A template method that returns a list of `Document`s,
        given a string-like representation of the xmi"""
        raise NotImplementedError()

    def _extract_passages(self, xmi):
        """A template method that returns a list of `Passage`s,
        given a string-like representation of the xmi"""
        raise NotImplementedError()

    def _extract_keywords(self, xmi):
        """A template method that returns a list of `Keyword`s,
        given a string-like representation of the xmi"""
        raise NotImplementedError()
