#!/usr/bin/env python


class DocumentFactory(object):
    """Base class that all `Document` factories should derive from.

    Subclasses must implement `_init_document` and `_load_contents`
    """
    def __init__(self):
        pass

    def create_document(self, uri=None):
        """Creates a new Document, which calls various template methods"""
        contents = self._load_contents(uri=uri)
        doc = self._init_document(uri=uri, contents=contents)
        return doc

    def _init_document(self, uri=None, contents=None):
        """Factory method for creating a Document"""
        raise NotImplementedError()

    def _load_contents(self, uri=None):
        """A template method that returns the contents of the document"""
        raise NotImplementedError()
