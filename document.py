#!/usr/bin/env python


class Document(object):
    """Base class that all Documents should derive from"""
    def __init__(self, uid=None, uri=None, rank=None, contents=None):
        self.uid = uid
        self.uri = uri
        self.rank = rank
        self.contents = contents

    def as_js(self):
        """Returns the contents of the document as a json-like dict"""
        return {'contents': self.contents}

    def highlight(self, begin, end):
        """Highlights the specified span"""
        raise NotImplementedError()
