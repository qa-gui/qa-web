#!/usr/bin/env python

from collections import defaultdict

DELIMITER = '>'


class Node:
    """Represents a node in the data graph.

    Should NOT be used outside of graph_helper.py.
    Use `construct_graph` below."""
    phase_to_count = defaultdict(int)

    def __init__(self, name, phase=0):
        self.children = {}
        self.name = name
        self.full_name = ""
        self.phase = phase

    def insert(self, children):
        if not children:
            return
        first, rest = children[0], children[1:]

        if first in self.children:
            # Node already exists as child
            child_node = self.children[first]
        else:
            # Node doesn't yet exist as child
            next_phase = self.phase + 1
            child_node = Node(first, phase=next_phase)
            self.phase_to_count[next_phase] += 1
            if self.full_name:
                child_node.full_name = "%s%s%s" % (self.full_name,
                                                   DELIMITER, first)
            else:
                child_node.full_name = "%s" % (first)
            self.children[first] = child_node
        child_node.insert(rest)

    def return_struct(self):
        children = []
        for child_name, child_node in self.children.iteritems():
            children.append(child_node.return_struct())

        retval = {
            'name': self.name,
            'phase': self.phase,
            'abbrev_name': self.name[:20],
            'full_name': self.full_name,
        }

        if children:
            retval['children'] = children
        return retval


def construct_graph(trace_strs):
    split_traces = map(lambda s: s.split(DELIMITER), trace_strs)

    # Ugly hack to reset counts across different requests
    Node.phase_to_count = defaultdict(int)

    root = Node('/')
    for split_trace in split_traces:
        root.insert(split_trace)

    if Node.phase_to_count:
        breadth = max(Node.phase_to_count.itervalues())
    else:
        breadth = 1

    depth = len(Node.phase_to_count) + 1  # Include root
    return {
        'graph': root.return_struct(),
        'breadth': breadth,
        'depth': depth,
    }
