#!/usr/bin/env python

import urllib
import xml.etree.ElementTree as et

from document_factory import DocumentFactory
from trec_document import TrecDocument


class TrecDocumentFactory(DocumentFactory):
    """Document factory for the TREC BioQA task"""

    BASE_URL = 'http://peace.isri.cs.cmu.edu:9080/TRECGenomics/xmi/%s.xmi'
    CAS_NAMESPACE = 'http:///uima/cas.ecore'

    def _init_document(self, uri=None, contents=None):
        return TrecDocument(uri=uri, contents=contents)

    def _load_contents(self, uri):
        url = self.BASE_URL % uri
        xmi = urllib.urlopen(url).read()

        root = et.fromstring(xmi)
        node = root.find('./{%s}Sofa' % self.CAS_NAMESPACE)
        return node.attrib['sofaString']
